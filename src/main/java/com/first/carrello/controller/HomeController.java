package com.first.carrello.controller;

import com.first.carrello.model.Carrello;
import com.first.carrello.model.OggettoCarrello;
import com.first.carrello.model.Prodotto;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;


@RequiredArgsConstructor
@RestController
@EnableEurekaClient
public class HomeController {

    private final RestTemplate restTemplate;

    protected final EurekaClient eurekaClient;



    @PostMapping("/carrello/{idCarrello}/oggetto")
    public Carrello addItem(@PathVariable Integer idCarrello, @RequestBody OggettoCarrello item) {
        Carrello carrello = new Carrello();
        if (idCarrello != null && item != null && item.getIdCarrello() != null) {
            // get product details
            InstanceInfo instance = eurekaClient.getNextServerFromEureka("gateway_appname", false);
//            InstanceInfo instanceProdotto = eurekaClient.getNextServerFromEureka("prodotto_appname", false);

            String productCatalogUrl = instance.getHomePageUrl()+"prodotto/"+ item.getIdCarrello();

            Prodotto itemProduct = restTemplate.getForObject(productCatalogUrl, Prodotto.class);
            if (itemProduct != null && itemProduct.getIdProdotto() != null) {
//                 adding total item price in the shopping cart item
                item.setPrezzoTotale(itemProduct.getPrezzoUnitario() * item.getQuantita());
                return carrello.addItem(idCarrello, item);
            }
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "item product not found");
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "cart or item missing");
    }
}
