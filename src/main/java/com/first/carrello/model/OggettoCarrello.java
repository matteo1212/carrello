package com.first.carrello.model;

import lombok.Data;

@Data
public class OggettoCarrello {

    private Integer idCarrello;
    private Integer quantita;
    private Integer prezzoTotale;
}
