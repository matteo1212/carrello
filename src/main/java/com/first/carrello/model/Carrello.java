package com.first.carrello.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Carrello {

    private Integer idCarrello;
    private List<OggettoCarrello> oggettiCarrello = new ArrayList<>();

    public Carrello addItem(Integer idCarrello, OggettoCarrello oggettoCarrello){
        this.idCarrello = idCarrello;
        oggettiCarrello.add(oggettoCarrello);
        return this;
    }
}
