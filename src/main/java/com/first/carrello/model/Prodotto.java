package com.first.carrello.model;

import lombok.Data;

@Data
public class Prodotto {

    private Integer idProdotto;
    private Integer prezzoUnitario;
}
